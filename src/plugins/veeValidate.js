/* eslint-disable camelcase */
import Vue from 'vue'
import { ValidationProvider, ValidationObserver, extend } from 'vee-validate'
import {
  required,
  email,
  confirmed,
  alpha_spaces,
  integer,
  oneOf,
  max
} from 'vee-validate/dist/rules'

extend('required', {
  ...required,
  message: 'El campo es requerido'
})
extend('email', {
  ...email,
  message: 'Ingresa un email válido'
})

extend('confirmed', {
  ...confirmed,
  message: 'Los campos deben coincidir'
})

extend('alpha_spaces', {
  ...alpha_spaces,
  message: 'Ingresar solo letras'
})

extend('integer', {
  ...integer,
  message: 'Ingresar solo números'
})

extend('oneOf', {
  ...oneOf,
  message: 'Selecione una opción'
})

extend('max', {
  ...max,
  message: 'Superó el limite de caracteres'
})
Vue.component('ValidationProvider', ValidationProvider)
Vue.component('ValidationObserver', ValidationObserver)
