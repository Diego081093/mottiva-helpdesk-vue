import Vue from 'vue'
import moment from 'moment'
import VueMoment from 'vue-moment'

require('moment/locale/es')

moment.locale('es')

Vue.use(VueMoment, { moment })
