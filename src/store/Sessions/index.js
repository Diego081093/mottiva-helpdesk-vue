// IMPORT LIBRARIES
import Vue from 'vue'
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// IMPORT SERVICES
// store/Auth/index.js

import { HTTP } from '@/plugins/axios'

// const ls = new SecureLS({ isCompression: false })
const state = {
  S_SESSIONS: [],
  S_CASES: []
}
const getters = {
}
const mutations = {
  SET_DATA (state, params) {
    // console.log(params)
    Vue.set(state, params.destination, params.data)
  },
  PUSH_DATA (state, params) {
    state[params.destination].push(params.data)
  }
}
const actions = {
  async A_GET_SESSIONS ({ commit }, Params) {
    try {
      const { data } = await HTTP.get(`sessions?to=${Params.current}&offset=${Params.offset}&limit=${Params.limit}`)

      commit('SET_DATA', {
        destination: 'S_SESSIONS',
        data: data.data
      })
      return data.data
    } catch (error) {
      console.log('ERROR_LOGIN [ACTION]', error)
      throw error
    }
  },
  async A_GET_CASES ({ commit }, idSession) {
    try {
      const { data } = await HTTP.get(`sessionCases/${idSession}`)
      return data
    } catch (error) {
      console.error('ERROR_GET_CASES:', error)
      throw error
    }
  },
  async A_ASSIGN_SESSION ({ commit }, idSession) {
    try {
      const { data } = await HTTP.put(`sessions/${idSession}/assign`)
      return data
    } catch (error) {
      console.error('ERROR_ASSIGN_SESSION:', error)
      throw error
    }
  },
  async A_POST_CASE_SESSION ({ commit }, params) {
    try {
      const { data } = await HTTP.post(`sessions/${params.idSession}/case`, params)
      return data
    } catch (error) {
      console.error('ERROR_POST_CASE_SESSION:', error)
      throw error
    }
  },
  async A_POST_CASE_CANCEL_SESSION ({ commit }, params) {
    try {
      const { data } = await HTTP.put(`sessions/${params.idSession}/cancel`, params)
      return data
    } catch (error) {
      console.error('ERROR_POST_CASE_SESSION:', error)
      throw error
    }
  },
  async A_POST_CASE_REPLACE_SESSION ({ commit }, params) {
    try {
      const { data } = await HTTP.put(`sessions/${params.idSession}/replace`, params)
      return data
    } catch (error) {
      console.error('ERROR_POST_CASE_SESSION:', error)
      throw error
    }
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations

  /*
    plugins: [createPersistedState({
    storage: {
      getItem: key => ls.get(key),
      setItem: (key, value) => ls.set(key, value),
      removeItem: key => ls.remove(key)
    }
  })]
  */
}
