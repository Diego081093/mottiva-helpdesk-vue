import Vue from 'vue'
import Vuex from 'vuex'
// IMPORT plugins
// import createPersistedState from 'vuex-persistedstate'
// import SecureLS from 'secure-ls'
// import of modules
import Profile from './Profile/index'
import Auth from './Auth/index'
import Sessions from './Sessions/index'
import emotionRoom from './emotionRoom/index'
import Blocked from './Blocked/index'
import Mottivateca from './Mottivateca/index'
import Benefit from './Benefit/index'
import Statistics from './Statistics/index'
import Institution from './Institution/index'
import Patients from './Patients/index'
import Chat from './Chat/index'
import Psychologist from './Psychologist/index'
import Protocols from './Protocols/index'
import Thematic from './Thematic/index'
import Method from './Method/index'
import Training from './Training/index'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    Auth,
    Profile,
    Sessions,
    emotionRoom,
    Blocked,
    Mottivateca,
    Benefit,
    Statistics,
    Institution,
    Patients,
    Chat,
    Psychologist,
    Protocols,
    Thematic,
    Method,
    Training
  }
})
/**
 * THIS FORMAT NAMED FUNCTIONS :
 * State      -> S_
 * Mutation   -> M_
 * Action     -> A_
 * Getter     -> G_
 *
 * FORMAT FUNCTION LOGICS
 * SET      -> setting data in state
 * PUSH     -> pushing data in array state
 *
 * DEFAULT MUTATIONS
 * SET_DATA     -> set data in element state
 * PUSH_DATA    -> push data in element state
 */
